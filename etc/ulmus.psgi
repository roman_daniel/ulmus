# sample PSGI app for Ulmus
use common::sense;
use Plack::Builder;

use Path::Class;
use lib ''. dir(file(__FILE__)->absolute->parent->parent, 'app', 'lib');
use Ulmus::IOC;

# lazily loaded IOC root
my $root = Ulmus::IOC->root;
my $app = $root->fetch('Webapp/app')->get;

# the application is mounted twice (just a proof of concept)
builder {
    mount '/garden/' => $app;
    mount '/'        => $app;
};


# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
