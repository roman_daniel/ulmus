<%class>
has node_rows       => (is => 'ro', required => 1, isa => 'ArrayRef');
has created_node_id => (is => 'ro', required => 0);
has add_node_form  => (is => 'ro', required => 1);
has colspan        => (is => 'ro', required => 1);
</%class>

<h1>Tree manager</h1>

% my $created_node_id = $.created_node_id;
<table id="tree-leveled">
<tbody>
% # to silence validator, my first row contains all the columns
% if ($.colspan > 1){
<tr class="initializer">
% for ( 1 .. $.colspan ){
    <td></td>
% }
</tr>
% } 

% for my $row ( @{$.node_rows} ){
	<tr>
% for my $cell ( @$row ){   
<%perl>
    my $colspan = $cell->{colspan};
    my $rowspan = $cell->{rowspan};
    my $node    = $cell->{node};
    my $created = $created_node_id && $created_node_id == $node->{node_id};
</%perl>
    <td\
% if ($colspan > 1){
 colspan="<% $colspan %>"\
% }
% if ($rowspan > 1){
 rowspan="<% $rowspan %>"\
% }
% if ($created){
 class="node-created"\
% }
>
% if ($node){
    P: <% $node->{parent_node_id} || 'NONE' %>  ID: <% $node->{node_id} %>
% }
    </td>
% }
	</tr>
% }
</tbody>
</table>

<& /component/add_node_form.mc, %{$.add_node_form} &>

% # vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
