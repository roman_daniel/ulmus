<%class>
has create_tree_form  => (is => 'ro', required => 1);
</%class>

<h1>Create tree</h1>

<& /component/create_tree_form.mc, %{$.create_tree_form} &>

% # vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
