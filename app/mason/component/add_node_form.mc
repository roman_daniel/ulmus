<%class>
has action => (is => 'ro', required => 1);
</%class>


<script type="text/javascript"> 
$(function(){
    function displayErrors(errors){
        $.each(errors, function(name, error){
            $('#nodeform label.error#' + name + '-error').text(error).show();
        });
    }

    $('#nodeform').ajaxForm({
        beforeSubmit: function(data, self){
            $('label.error', self).hide().text('');
        },
        success: function(){
            window.location.reload(true);
        },
        error: function(o){
            /* pass errors to  */
            if (o.status === 422){
                if ( o.responseJSON && o.responseJSON.invalid_fields ){
                    displayErrors(o.responseJSON.invalid_fields);
                    return;
                }
            }
            displayErrors({ nodeid: "Unexpected error occured" });
        }
    });
});
</script>

<form method="POST" action="<% $.action %>" id="nodeform">
    <div class="row">
        <p>To create a new row, supply the id of its parent in the tree</p>
        <label for="parent_node_id">Parent node id</label>
        <input name="parent_node_id" id="parent_node_id">
    </div>
    <div class="row">
        <label id="parent_node_id-error" class="error" for="parent_node_id"></label>
        <input type="submit" value="Add node" class="button-primary" >
    </div>
</form>
% # vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
