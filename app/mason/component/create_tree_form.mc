<%class>
has action => (is => 'ro', required => 1);
</%class>

<form method="POST" action="<% $.action %>" id="nodeform">
    <div class="row">
	    <input type="submit" value="Create new tree" class="button-primary" >
    </div>
</form>

% # vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
