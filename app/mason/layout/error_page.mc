<%class>
has reason       => (is =>'ro', required => 1);
has title        => (is => 'ro', default => 'Ulmus - the tree mapper');
has status       => ( is => 'ro', required => 1 );
has user_message => (is => 'ro', required => 1 );

# creates the uri for assets (css, js, images)
has static_uri_maker => (
    is       => 'ro',
    required => 1,
    isa      => 'CodeRef',
    traits   => ['Code'],
    handles  => { uri_for_static => 'execute' },
);

</%class><!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs  -->
  <meta charset="utf-8">
  <title><% $.title %></title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSS -->
  <link rel="stylesheet" href="<% $.uri_for_static('css/normalize.css') %>">
  <link rel="stylesheet" href="<% $.uri_for_static('css/skeleton.css') %>">
  <link rel="stylesheet" href="<% $.uri_for_static('css/ulmus.css') %>">
  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
  <script type="text/javascript" src="<% $.uri_for_static('js/jquery.form.min.js') %>"></script>
</head>
<body>

<!-- Primary Page Layout  -->
  <div class="container">
    <div class="row">
      <div class="ten columns" style="margin-top: 10%">
	    <h1><% $.status %> <% $.reason %></h1>
        <p><% $.user_message %></p>
      </div>
    </div>
  </div>

<!-- End Document -->
</body>
</html>
% # vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
