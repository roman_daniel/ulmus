package Ulmus::Webapp::App::Controller;
use Moose;

# ABSTRACT: glue together (request building, controller call and rendering)

use Carp;
use List::Util qw(pairmap);

has controller_call => (
    is       => 'ro',
    required => 1,
    isa      => 'CodeRef',
    traits   => ['Code'],
    handles  => { call_controller => 'execute' },
);

has request_factory => (
    is       => 'ro',
    required => 1,
    isa      => 'CodeRef',
    traits   => ['Code'],
    handles  => { build_request => 'execute' },
);

# not ready yet
# has body_parser => ( is => 'ro', required => 1, handles => { '_parse_body' => 'parse' } );

# processed the data before renderer  - supplies templates, etc.
my $identity = sub {
    my ( $env, $res ) = @_;
    return $res;
};

has normalizer => (
    is      => 'ro',
    isa     => 'CodeRef',
    traits  => ['Code'],
    handles => { normalize => 'execute' },
    default => sub { $identity },
);

has renderer => ( is => 'ro', required => 1, isa => 'Object', handles => ['render'] );

sub parse_body {
    my ($this, $env) = @_;

    # if supplied body_parser is a coderef, which receives actual parser
    return undef if ! grep { $env->{REQUEST_METHOD} eq $_ } qw(POST PUT);
    return $this->_parse_body( $env );
}

sub call {
    my ( $this, $env, @match_args ) = @_;

    my $req = $this->build_request(
        env => $env,

        #    data   => $this->parse_body($env),
    );
    return $this->normalize_response( $req,
        $this->call_controller( $req, @match_args ) );
}

sub normalize_response {
    my ( $this, $req, $res ) = @_;

    my $env = $req->env;

    return $res if 'ARRAY' eq ref $res || '';
    return $this->render($env, $this->normalize($env, $res)) if 'HASH' eq ref $res || '';
    confess "Unnormalizable response: $res";
}

__PACKAGE__->meta->make_immutable;
no Moose; 1;

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:

