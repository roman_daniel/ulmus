package Ulmus::Webapp::Exception::Role::Status::UnprocessableEntity;
use Moose::Role;

with(
    'HTTP::Throwable',
    'HTTP::Throwable::Role::BoringText',
);

sub default_status_code { 422 }
sub default_reason      { 'Unprocessable Entity' }

no Moose::Role; 1;

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:

