package Ulmus::Webapp::Exception::Base;
use Moose;

# http exception can have arbitrary data
has data => (is => 'ro', required => 0, predicate => 'has_data');

has user_message => (is => 'ro', required => 0, predicate => 'has_user_message');

__PACKAGE__->meta->make_immutable;
no Moose; 1;

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
