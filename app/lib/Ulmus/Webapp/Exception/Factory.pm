package Ulmus::Webapp::Exception::Factory;
use Moose;

extends 'HTTP::Throwable::Factory';

# we have our own 422
around roles_for_ident => sub {
    my ( $orig, $this, $ident ) = @_;

    return $ident eq 'UnprocessableEntity'
        ? "Ulmus::Webapp::Exception::Role::Status::UnprocessableEntity"
        : $orig->( $this, $ident );
};

around base_class => sub {
    return 'Ulmus::Webapp::Exception::Base';
};

__PACKAGE__->meta->make_immutable;
no Moose; 1;
# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
