package Ulmus::Webapp::Statuses;
use strict;
use warnings;

# ABSTRACT: simple status (hash returning) helpers

use Exporter 'import';
our @EXPORT = qw(Ok Created NoContent UnprocessableEntity);

sub Ok {
    my ( $data, %args ) = @_;
    return { status => 200, data => $data, %args, };
}

sub Created {
    my ( $data, %args ) = @_;
    return { status => 201, data => $data, %args, };
}

# no content has no body
sub NoContent {
    my (%args) = @_;

    return { status => 204, %args, };
}

# unprocessable entity is returned, instead of thrown :-(
sub UnprocessableEntity {
    my ($data, %args) = @_;

    return { status => 422, data => $data, %args };
}

1;

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=96:syntax=perl
