package Ulmus::Webapp::Controller::Forest;
use Moose;

# ABSTRACT: shows tree and add tree

use Ulmus::Webapp::Statuses;
use Try::Tiny;
use Safe::Isa;

# model
has forest => ( is => 'ro', required => 1 );

# I need something which can txn_do
has transactions => (is => 'ro', required => 1, handles => ['txn_do']);

# just serve the form
sub create_GET {
    my ( $this, $c ) = @_;
    
    return Ok(
        {   body => {
                create_tree_form => { action => $c->uri_for( 'tree_create'),  }
            }
        }
    );
}

sub create_POST {
    my ( $this, $c ) = @_;

    my $tree_id;
    $this->txn_do(
        sub {
            $tree_id = $this->forest->create( {} );
        }
    );
    $c->throw( SeeOther =>
            { location => '' . $c->uri_for( 'tree_view', [$tree_id] ), } );
}

__PACKAGE__->meta->make_immutable;
no Moose;
1;

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
