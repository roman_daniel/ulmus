package Ulmus::Webapp::Controller::Tree;
use Moose;

# ABSTRACT: shows tree and add tree

use Ulmus::Webapp::Statuses;
use Try::Tiny;
use Safe::Isa;

has tree_model_factory => (
    is       => 'ro',
    required => 1,
    isa      => 'CodeRef',
    traits   => ['Code'],
    handles  => { create_tree => 'execute' }
);

# I need something which can txn_do
has transactions => ( is => 'ro', required => 1, handles => ['txn_do'] );

sub require_tree {
    my ( $this, $c, $tree_id ) = @_;

    $tree_id =~ /^\d+$/
        or
        $c->throw( NotFound => { user_message => "Page not exists here", } );

    my $tree = $this->create_tree($tree_id);
    $tree->exists
        or $c->throw(
        NotFound => { user_message => "There is no tree with id $tree_id", }
        );
    return $tree;
}

sub view_GET {
    my ( $this, $c, $tree_id ) = @_;

    my $tree = $this->require_tree( $c, $tree_id );
    my $rows = $tree->compose_leveled();

    return Ok(
        {   body => {
                node_rows       => $rows,
                colspan         => @$rows ? $rows->[0][0]{colspan} : 1,
                created_node_id => delete $c->session->{created_node_id},
                add_node_form =>
                    { action => $c->uri_for( 'tree_add_node', [$tree_id] ), }
            }
        }
    );
}

sub _invalid_parent_node {
    my ( $this, $c, $message ) = @_;

    return UnprocessableEntity(
        { invalid_fields => { parent_node_id => $message } } );
}

sub add_node_POST {
    my ( $this, $c, $tree_id ) = @_;

    my $parent_node_id = $c->body_parameters->{parent_node_id}
        or return $this->_invalid_parent_node( $c, "Parent node id missing" );

    $parent_node_id =~ /^(\d+)$/
        or return $this->_invalid_parent_node( $c,
        "Parent node must be a number" );

    my $tree = $this->require_tree( $c, $tree_id );
    my $node_id;
    eval {
        $this->txn_do(
            sub {
                $node_id = $tree->add_child_node($parent_node_id);
            }
        );
    };
    if ( my $exc = $@ ) {

        # rewrap the specific exception from the model
        return $this->_invalid_parent_node( $c, $exc->message )
            if $exc->$_isa('Ulmus::Model::Exception::InvalidParentNode');

        # all other exceptions are thrown out
        die $exc;
    }

    # passing
    $c->session->{created_node_id} = $node_id;
    return Ok( { ok => 'OK' } );
}

__PACKAGE__->meta->make_immutable;
no Moose;
1;

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
