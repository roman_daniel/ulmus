package Ulmus::Webapp::Request;
use Moose;

# ABSTRACT: current request with routing and exceptions

extends 'Web::Request';

has router => (is => 'ro', required => 1);

has exception_factory => (is => 'ro', required => 1, handles => ['throw']);

sub session { shift()->env->{'psgix.session'} }

# creates URI for the route 
sub uri_for {
    my ( $this, $name, $args, $query_form, $fragment ) = @_;

    my $uri_base = $this->script_name || '/';
    $uri_base .= '/' unless $uri_base =~ m+/$+;

    my $path = $this->router->path_for( $name, $args );
    my $uri = URI->new( $uri_base . $path );

    if ($query_form) {
        $uri->query_form($query_form);
    }
    if (defined $fragment){
        $uri->fragment( $fragment );
    }
    # returns text so it can be passed directly to JSON
    return "$uri";
}

__PACKAGE__->meta->make_immutable;
no Moose; 1;

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=96:syntax=perl
