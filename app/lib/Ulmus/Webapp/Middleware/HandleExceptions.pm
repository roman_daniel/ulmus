package Ulmus::Webapp::Middleware::HandleExceptions;
use Moose;

use Safe::Isa;
use Plack::Util;

has renderer => ( is => 'ro', required => 1 );

my $identity = sub { my ( $env, $res ) = @_; $res };
has normalizer => (
    is      => 'ro',
    isa     => 'CodeRef',
    traits  => ['Code'],
    handles => { normalize => 'execute' },
    default => sub {$identity},
);

has renderer =>
    ( is => 'ro', required => 1, isa => 'Object', handles => ['render'] );

sub wrap {
    my ( $class, $app, @args ) = @_;

    return $class->new(@args)->wrap_app($app);
}

sub wrap_app {
    my ( $this, $app ) = @_;

    return sub {
        my $env = shift;

        my $res = eval { $app->($env); };
        return $@ ? $this->handle_exception( $env, $@ ) : $res;
    };
}

sub handle_exception {
    my ( $this, $env, $exc ) = @_;

    # simplified
    if ( $exc->$_isa('Ulmus::Webapp::Exception::Base') ) {

        # our exception
        # redirects are textual
        return $exc->as_psgi
            if $exc->status_code >= 300 && $exc->status_code < 400;

        my @headers = @{ $exc->build_headers };
        my $hdrs    = Plack::Util::headers( \@headers );
        $hdrs->remove('Content-Type');

        # clearing the Content-Type text/plain
        return $this->_render_exception(
            $env,
            {   status  => $exc->status_code,
                headers => \@headers,
                data    => {
                    reason    => $exc->reason,
                    status    => $exc->status_code,
                    ( $exc->has_data ? %{ $exc->data } : () ),
                user_message => (
                      $exc->has_user_message
                    ? $exc->user_message
                    : 'The exception we do not know much about occured'
                ),
                },
            }
        );
    }
    else {
        warn "Internal error occured: $exc";
        return $this->_render_exception(
            $env,
            {   status => 500,
                data   => {
                    reason      => 'Internal server error',
                    status   => 500,
                    user_message =>
                        'Some very unpredictable exception occured, contact the admin',
                }
            }
        );
    }
}

sub _render_exception {
    my ( $this, $env, $res ) = @_;

    my $psgi = eval { $this->render( $env, $this->normalize( $env, $res ) ) };
    my $exc = $@ or return $psgi;

    # last resort when rendering failed
    warn "Internal error occured: $exc";
    return [
        500,
        [ 'Content-Type' => 'text/plain' ],
        [   'Very unpredictable internal error occured, contact administrator'
        ]
    ];
}

__PACKAGE__->meta->make_immutable;
no Moose;
1;

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
