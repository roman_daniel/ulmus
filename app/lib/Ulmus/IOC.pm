package Ulmus::IOC;
use common::sense;

# ABSTRACT: root of IOC tree

use Bread::Board::LazyLoader::Site;

1;

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
