use utf8;
package Ulmus::Schema::Result::Tree;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';
__PACKAGE__->table("tree");
__PACKAGE__->add_columns(
  "tree_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("tree_id");
__PACKAGE__->has_many(
  "edges",
  "Ulmus::Schema::Result::Edge",
  { "foreign.tree_id" => "self.tree_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "nodes",
  "Ulmus::Schema::Result::Node",
  { "foreign.tree_id" => "self.tree_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->might_have(
  "root_node",
  "Ulmus::Schema::Result::RootNode",
  { "foreign.tree_id" => "self.tree_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-04-25 12:23:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:A7FFdH5sZ6NRBZadNhxExQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
