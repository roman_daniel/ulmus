use utf8;
package Ulmus::Schema::Result::Node;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';
__PACKAGE__->table("node");
__PACKAGE__->add_columns(
  "node_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "tree_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "depth",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 0 },
);
__PACKAGE__->set_primary_key("node_id");
__PACKAGE__->might_have(
  "child_edge",
  "Ulmus::Schema::Result::Edge",
  {
    "foreign.child_node_id" => "self.node_id",
    "foreign.tree_id"       => "self.tree_id",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "parent_edges",
  "Ulmus::Schema::Result::Edge",
  {
    "foreign.parent_node_id" => "self.node_id",
    "foreign.tree_id"        => "self.tree_id",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->might_have(
  "root_node",
  "Ulmus::Schema::Result::RootNode",
  {
    "foreign.node_id" => "self.node_id",
    "foreign.tree_id" => "self.tree_id",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->belongs_to(
  "tree",
  "Ulmus::Schema::Result::Tree",
  { tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-04-25 12:47:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:BFI1qG0FFSyluirDn1wOMA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
