use utf8;
package Ulmus::Schema::Result::Edge;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';
__PACKAGE__->table("edge");
__PACKAGE__->add_columns(
  "edge_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "tree_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "parent_node_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "child_node_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("edge_id");
__PACKAGE__->add_unique_constraint("tree_id", ["tree_id", "child_node_id"]);
__PACKAGE__->belongs_to(
  "child_node",
  "Ulmus::Schema::Result::Node",
  { node_id => "child_node_id", tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);
__PACKAGE__->belongs_to(
  "parent_node",
  "Ulmus::Schema::Result::Node",
  { node_id => "parent_node_id", tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);
__PACKAGE__->belongs_to(
  "tree",
  "Ulmus::Schema::Result::Tree",
  { tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-04-25 12:38:29
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:m1OlxK/cAcTiVq+xfqRkcg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
