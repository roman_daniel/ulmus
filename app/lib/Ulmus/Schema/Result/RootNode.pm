use utf8;
package Ulmus::Schema::Result::RootNode;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';
__PACKAGE__->table("root_node");
__PACKAGE__->add_columns(
  "tree_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "node_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
__PACKAGE__->set_primary_key("tree_id", "node_id");
__PACKAGE__->add_unique_constraint("tree_id", ["tree_id"]);
__PACKAGE__->belongs_to(
  "node",
  "Ulmus::Schema::Result::Node",
  { node_id => "node_id", tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);
__PACKAGE__->belongs_to(
  "tree",
  "Ulmus::Schema::Result::Tree",
  { tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-04-25 11:49:49
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:BjP3ttvYdRXcIkjtKiRNDQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
