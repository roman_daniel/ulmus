package Ulmus::Model::Exception::InvalidParentNode;
use Moose;

# ABSTRACT: recognizable model exception
extends 'Throwable::Error';

has parent_node_id => (is => 'ro', required => 1);

__PACKAGE__->meta->make_immutable;
no Moose; 1;

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:

