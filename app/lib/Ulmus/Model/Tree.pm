package Ulmus::Model::Tree;
use Moose;

# ABSTRACT: abstraction over tree

use List::Util qw(max);
use Carp qw(croak);
use Ulmus::Model::Exception::InvalidParentNode;

has tree_id => ( is => 'ro', required => 1 );

has schema => ( is => 'ro', required => 1, 'handles' => ['resultset'] );

sub exists {
    my $this = shift;

    return $this->schema->resultset('Tree')->find( $this->tree_id ) ? 1 : 0;
}

sub compose_leveled {
    my $this = shift;

    my ($root_node_id)
        = map { $_->node_id }
        $this->resultset('RootNode')->search( { tree_id => $this->tree_id } )
        or return [];

    # loading all nodes - if document root is set inside, we respect it
    my $rs = $this->resultset('Node')->search(
        {   'me.tree_id' => $this->tree_id,
            'me.node_id' => { '>=' => $root_node_id }
        },
        {   order_by => { '-asc' => ['node_id'] },
            prefetch => ['child_edge'],
        }
    );

    my %nodes;
    my $max_depth = 0;
    my %children_for;
    while ( my $node_row = $rs->next ) {
        my $node_id = $node_row->node_id;

        my $node = $nodes{$node_id} = {
            node_id => $node_id,
            depth   => 1,
        };

        # parent out of $root_node_id is not accepted
        my ($parent_node_id) = grep { $_ >= $root_node_id }
            map { $_->parent_node_id } grep {$_} $node_row->child_edge;
        if ($parent_node_id) {

            my $parent_node = $nodes{$parent_node_id};
            my $depth = $node->{depth} = $parent_node->{depth} + 1;
            $node->{parent_node_id} = $parent_node_id;
            push @{ $children_for{$parent_node_id} }, $node_id;
            if ( $depth > $max_depth ) {
                $max_depth = $depth;
            }
        }
    }

    my @lines;
    my $spread = sub {

        # passing self to prevent circular reference
        my ( $spread, $node_id, $left ) = @_;

        my $node  = $nodes{$node_id};
        my $depth = $node->{depth};

        my $current = $left - 1;
        for my $child_node_id ( @{ $children_for{$node_id} || [] } ) {
            $current = $spread->( $spread, $child_node_id, $current + 1 );
        }

        my $right = max( $current, $left );
        push @{ $lines[ $depth - 1 ] }, {
            node => $node,
            colspan => $right - $left + 1,
            # if no children and not last row we put an empty block
            rowspan => ( $current < $left && $depth < $max_depth ? $max_depth - $depth + 1 : 1),
        };
        return $right;
    };
    $spread->( $spread, $root_node_id, 1 );
    return \@lines;
}

sub throw_invalid_node {
    my $this = shift;
    Ulmus::Model::Exception::InvalidParentNode->throw(@_);
}

sub add_child_node {
    my ( $this, $parent_node_id ) = @_;

    my $nodes       = $this->resultset('Node');
    my $parent_node = $nodes->find($parent_node_id)
        or $this->throw_invalid_node(
        parent_node_id => $parent_node_id,
        message        => "Node $parent_node_id not found"
        );
    $parent_node->tree_id == $this->tree_id
        or $this->throw_invalid_node(
        parent_node_id => $parent_node_id,
        message        => "Node $parent_node_id belongs to a different tree"
        );

    my $child_node = $nodes->create( { tree_id => $this->tree_id } );
    $parent_node->parent_edges->create(
        { child_node_id => $child_node->node_id } );
    return $child_node->id;
}

__PACKAGE__->meta->make_immutable;
no Moose;
1;

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
