package Ulmus::Model::Forest;
use Moose;

# ABSTRACT: abstraction over (many) trees

use Carp qw(croak);

has schema => ( is => 'ro', required => 1, 'handles' => ['resultset'] );

# creates new (empty) tree
sub create {
    my ($this) = @_;

    my $tree = $this->resultset('Tree')->create( {} );
    my $root_node = $tree->nodes->create({});
    $this->resultset('RootNode')->create({
        tree_id => $tree->tree_id,
        node_id => $root_node->node_id,
    });

    return $tree->tree_id;
}

__PACKAGE__->meta->make_immutable;
no Moose; 1;
# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
