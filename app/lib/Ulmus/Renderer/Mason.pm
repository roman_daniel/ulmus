package Ulmus::Renderer::Mason;
use Moose;

# ABSTRACT: rendering response hash via Mason template

use Plack::Util;
use Carp qw(croak);

has interp => ( is => 'ro', required => 1 );

has content_type => ( is => 'ro', default => 'text/html;charset=utf-8' );

# render gets hash { status => $status, headers => $headers, data => $data }
sub render {
    my ( $this, $env, $res ) = @_;

    my $body    = $this->render_body( $res );
    utf8::encode($body);

    my @headers = map { $_ ? @$_ : () } $res->{headers};
    my $hdrs = Plack::Util::headers( \@headers );
    $hdrs->set( 'Content-Type' => $this->content_type )
        if !$hdrs->exists('Content-Type');
    $hdrs->set( 'Content-Length' => length($body) );
    utf8::encode($_) for @headers;
    return [ $res->{status} || 200, \@headers, [ $body ], ];
}

sub render_body {
    my ( $this, $res ) = @_;


    my $data     = $res->{data} or return '';
    my $template = $res->{template}
        or croak "No template passed for mason rendering";
    return $this->interp->run($template, %$data)->output;
}

__PACKAGE__->meta->make_immutable;
no Moose; 1;

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
