package Ulmus::Renderer::JSON;
use Moose;

# ABSTRACT: rendering response hash to JSON
use Plack::Util;
use Carp qw(croak);
use JSON ();

has content_type => ( is => 'ro', default => 'application/json;charset=utf-8' );

has json => (
    is       => 'ro',
    required => 1,
    default  => sub { JSON->new },
    handles  => { to_json => 'encode' },
);

# render gets hash { status => $status, headers => $headers, data => $data }
sub render {
    my ( $this, $env, $res ) = @_;

    my $body    = $this->render_body( $res );
    utf8::encode($body);

    my @headers = map { $_ ? @$_ : () } $res->{headers};
    my $hdrs = Plack::Util::headers( \@headers );
    $hdrs->set( 'Content-Type' => $this->content_type )
        if !$hdrs->exists('Content-Type');
    $hdrs->set( 'Content-Length' => length($body) );
    utf8::encode($_) for @headers;
    return [ $res->{status} || 200, \@headers, [ $body ], ];
}

sub render_body {
    my ( $this, $res ) = @_;

    my $data     = $res->{data} or return '';
    return $this->to_json($data);
}

__PACKAGE__->meta->make_immutable;
no Moose; 1;

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
