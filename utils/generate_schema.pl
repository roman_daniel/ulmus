use common::sense;
use DBIx::Class::Schema::Loader qw(make_schema_at);

use Data::Dump qw(pp);

make_schema_at(
    'Ulmus::Schema',
    {   debug          => 1,
        dump_directory => './app/lib',

        #constraint => '',
        generate_pod => 0,
        rel_name_map => sub {
            my $rel = shift;

            my ($self, $foreign, $self_cols, $foreign_cols) = @$rel{ qw(
                local_moniker remote_moniker local_columns remote_columns) };

            if ($self eq 'Edge' && $foreign eq 'Node'){
                return 'parent_node' if $self_cols->[1] eq 'parent_node_id';
                return 'child_node'  if $self_cols->[1] eq 'child_node_id';
            }

            if ($self eq 'Node' && $foreign eq 'Edge'){
                return 'parent_edges' if $foreign_cols->[1] eq 'parent_node_id';
                return 'child_edge'  if $foreign_cols->[1] eq 'child_node_id';
            }

            return $rel->{name};
        },
    },
    [ "dbi:mysql:ulmus", 'saperda', 'punctata', { RaiseError => 1,} ],
);

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
__END__
Ulmus::Schema::Result::Edge->table("edge");
Ulmus::Schema::Result::Edge->add_columns(
  "edge_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "tree_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "parent_node_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "child_node_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
Ulmus::Schema::Result::Edge->set_primary_key("edge_id");
Ulmus::Schema::Result::Edge->add_unique_constraint("tree_id", ["tree_id", "child_node_id"]);
Ulmus::Schema::Result::Node->table("node");
Ulmus::Schema::Result::Node->add_columns(
  "node_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "tree_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "depth",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 0 },
);
Ulmus::Schema::Result::Node->set_primary_key("node_id");
Ulmus::Schema::Result::RootNode->table("root_node");
Ulmus::Schema::Result::RootNode->add_columns(
  "tree_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "node_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);
Ulmus::Schema::Result::RootNode->set_primary_key("tree_id", "node_id");
Ulmus::Schema::Result::RootNode->add_unique_constraint("tree_id", ["tree_id"]);
Ulmus::Schema::Result::Tree->table("tree");
Ulmus::Schema::Result::Tree->add_columns(
  "tree_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
);
Ulmus::Schema::Result::Tree->set_primary_key("tree_id");
Ulmus::Schema::Result::Edge->belongs_to(
  "child_node",
  "Ulmus::Schema::Result::Node",
  { node_id => "child_node_id", tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);
Ulmus::Schema::Result::Edge->belongs_to(
  "parent_node",
  "Ulmus::Schema::Result::Node",
  { node_id => "parent_node_id", tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);
Ulmus::Schema::Result::Edge->belongs_to(
  "tree",
  "Ulmus::Schema::Result::Tree",
  { tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);
Ulmus::Schema::Result::Node->might_have(
  "child_edge",
  "Ulmus::Schema::Result::Edge",
  {
    "foreign.child_node_id" => "self.node_id",
    "foreign.tree_id"       => "self.tree_id",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);
Ulmus::Schema::Result::Node->has_many(
  "parent_edges",
  "Ulmus::Schema::Result::Edge",
  {
    "foreign.parent_node_id" => "self.node_id",
    "foreign.tree_id"        => "self.tree_id",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);
Ulmus::Schema::Result::Node->might_have(
  "root_node",
  "Ulmus::Schema::Result::RootNode",
  {
    "foreign.node_id" => "self.node_id",
    "foreign.tree_id" => "self.tree_id",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);
Ulmus::Schema::Result::Node->belongs_to(
  "tree",
  "Ulmus::Schema::Result::Tree",
  { tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);
Ulmus::Schema::Result::RootNode->belongs_to(
  "node",
  "Ulmus::Schema::Result::Node",
  { node_id => "node_id", tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);
Ulmus::Schema::Result::RootNode->belongs_to(
  "tree",
  "Ulmus::Schema::Result::Tree",
  { tree_id => "tree_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);
Ulmus::Schema::Result::Tree->has_many(
  "edges",
  "Ulmus::Schema::Result::Edge",
  { "foreign.tree_id" => "self.tree_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
Ulmus::Schema::Result::Tree->has_many(
  "nodes",
  "Ulmus::Schema::Result::Node",
  { "foreign.tree_id" => "self.tree_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
Ulmus::Schema::Result::Tree->might_have(
  "root_node",
  "Ulmus::Schema::Result::RootNode",
  { "foreign.tree_id" => "self.tree_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
Dumping manual schema for Ulmus::Schema to directory ./app/lib ...
Schema dump completed.
