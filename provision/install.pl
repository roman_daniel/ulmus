#!/usr/bin/perl

use strict;
use warnings;
use File::Path qw(mkpath);
use File::Temp qw(tempdir);

# installing app perl via system perl
my $perl_version       = '5.20.0';
my $perl_root_dir      = "/opt/perl";
my $configure_params
    = "-des -Dprefix=${perl_root_dir} -Doptimize='-O2' -Duselargefiles";

#my $perl_owner         = 'vagrant';

# database configuration 
my $db_name   = 'ulmus';
my $db_user   = 'saperda';
my $db_password = 'punctata';

my $source_dir = "/vagrant";

my $perl_download_file = "perl-${perl_version}.tar.gz";
my $perl_bin_dir = "$perl_root_dir/bin";
my $perl_bin     = "$perl_root_dir/bin/perl";
my $cpanm_bin    = "$perl_bin_dir/cpanm";


-d $perl_root_dir or mkpath($perl_root_dir);

if ( !-f $perl_bin ) {
    my $tmpdir = tempdir( CLEANUP => 1 );
    my $builddir = "$tmpdir/perl-${perl_version}";
    for my $cmd (
        "cd $tmpdir; curl --insecure -O http://www.cpan.org/src/5.0/${perl_download_file}",
        "cd $tmpdir; tar -xzvf ${perl_download_file}",
        "cd $builddir; ./Configure ${configure_params}",
        "cd $builddir; make",
        "cd $builddir; make install",
        )
    {
        run_cmd($cmd);
    }
}

if ( !-f $cpanm_bin ) {

    # installing cpanm
    run_cmd("curl -L http://cpanmin.us | ${perl_bin} - --self-upgrade");
}

run_cmd("yum -y install mariadb-server");
run_cmd("yum -y install mariadb");
run_cmd("yum -y install mariadb-devel");

run_cmd("echo 'export PATH=$perl_bin_dir:\$PATH' > /etc/profile.d/perl.sh");

# installing deps
run_cmd("$cpanm_bin --installdeps $source_dir");

# starting Maria DB
run_cmd("service mariadb start");


if (grep { $_ eq $db_name }
    map  {/(\S+)/} qx{mysql -sN -e 'show databases' }
    )
{
    warn "Database $db_name already exists";
}
else {
    run_cmd(
        "mysql  -e 'create database ${db_name} default character set utf8'");
}

if ( grep { $_ eq $db_user }
    map {/(\S+)/}
    qx{mysql mysql -sN -e "select user from user where user='${db_user}'"} )
{
    warn "User $db_user already exists\n";
}
else {
    run_cmd(
        join ' ',
        'mysql ',
        map { ( '-e', "\"$_;\"" ); }
            "create user '$db_user'\@'localhost' identified by '$db_password'",
        "grant all privileges on *.* to '$db_user'\@'localhost'",
        "flush privileges"
    );
}

if ( grep { $_ } qx{mysql $db_name -sN -e 'show tables'}){
	warn "Structure already loaded\n";
}
else {
        run_cmd("mysql $db_name < $source_dir/sql/structure.sql ");
}

sub run_cmd {
    my ($cmd) = @_;
    warn "Running '$cmd'";
    system($cmd) == 0
        or die "Running '$cmd' failed with: $!";
}

# vim: expandtab:shiftwidth=4:tabstop=4:softtabstop=0:textwidth=78:
