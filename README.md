# Ulmus - over engineered tree manager application #

### Why Ulmus ###

Ulmus (elm) was the first tree which came to my mind, when I tried to find a name for Tree manager application. 

### Installation ###

#### Inside Vagrant box #####

The preferred way of running Ulmus app is inside Vagrant box. The distribution comes with Vagrantfile and a provisioning script. To setup the box you need [vagrant tool](http://docs.vagrantup.com/v2/) and a virtualizer (for example [virtualbox](https://www.virtualbox.org/wiki/VirtualBox)). I recommend to use the latest version of vagrant (>= 1.7.2).

To setup the box simply enter the Ulmus distribution directory, and run:

```
vagrant up
```

Box setup (downloading the image) and provisioning may take some time (20 - 30minutes). During provisioning the Perl language is compiled from sources and quite a number of CPAN modules is installed. 

Failed provisioning in some cases may be called repeatedly:

```
vagrant provision
```

If the provisioning is successful, you have a vagrant box with:

 * CentOS 7 Linux distribution installed
 * Perl 5.20.0 (installed from sources) installed into `/opt/perl`, together with `cpanm` tool
 * neccessary Perl modules installed 
 * Maria db client tool (mysql) installed
 * Maria db server installed and running
 * Database `ulmus` which can be accessed with user `saperda`, password `punctata`

You can check the database from the box (`vagrant ssh`)

```
mysql -u saperda -ppunctata ulmus
```

#### Without vagrant box ####

This way of running Ulmus was not tested thoroughly, but it should work as well. To run Ulmus in your own environment, you need:

 * Perl environment where you can install modules from CPAN, with some recent Perl and cpanm tool
 * mysql (Maria DB) server and client

From the root of your Ulmus directory, install necessary perl modules via

```
cpanm --installdeps ./
```

or (if you want to speed up the process by not running tests of CPAN modules)

```
cpanm -n --installdeps ./
```

Create the `ulmus` database and `saperda` user, the user (and password) for mysql must be set according to your environment:

```
mysql -u root  -e 'create database ulmus default character set utf8';
mysql -u root -e "create user saperda@localhost identified by 'punctata'";
mysql -u root -e "grant all privileges on *.* to 'saperda'@'localhost'";
mysql -u root -e "flush privileges";
```

Load the database structure 

```
mysql -u saperda -ppunctata ulmus < ./sql/structure.sql
```

### Running the Ulmus application  ###

#### From the vagrant box ####

You can run the application in your box in foreground using 

```
vagrant ssh -c 'plackup -p 5000 /vagrant/etc/ulmus.psgi' -- -L 5000:localhost:5000
```

Ulmus app can be found here `http://localhost:5000/` or here `http://localhost:5000/garden`


#### Outside of Vagrant box ####

From your distribution directory, run:

```
plackup -p 5000 ./etc/ulmus.psgi
```

Again your app can be found here `http://localhost:5000/` or here `http://localhost:5000/garden`

### Architectural notes ###

The application is quite overengineered for its size. I did some copy, modify and paste of my own work from other projects. 
On the other hand the app is far from being complete. There are no automated tests, no logging, exception pages are not very informative, 
there is no runner (the appis run via plackup), ...

#### Aplication design (frontend)

The design of the application is very spartan, and borrowed from skeleton [Skeleton](http://getskeleton.com).

#### Database model ####

The database consists of 4 tables (see `sql/structure.sql`), there is 

  * tree - grouping the nodes (there may be more than one tree in db)
  * node - node under the tree
  * edge - parent/child relationship
  * root_node - determining the root node of the tree

I use this design to avoid circular references of foreign keys (even node to node).

#### Web application ####

Ulmus is a PSGI application, which gives wide possibilities to wrap it by middlewares in your particular environment.

Inside the application is MVC like structured, where:

 * models are arbitrary Moose objects manipulating with database rows via DBIx::Class::Schema
 * controllers are arbitrary Moose objects, which have direct access to models (via IOC). Each controller method either fails or return hash with status, headers, data. Such response is later rendered by renderer
 * views are Mason2 templates 

I intentionally have not used any Web framework here. Instead I strive to use independent components from CPAN (DBIx::Class, Mason, Web::Request, Plack, Router::Pygmy, HTTP::Throwable), each of them having ssmaller, cleaner interface and wire them together **explicitly** via IOC. Something like frameworkless framework.

#### Database access ####

Database is accessed primarily via (generated) DBIx::Class::Schema. I use DBIx::Class as a superb query generator, but I do not want to use it as a model directly (ORM != model). I have quite a lot of bad personal experience with such attitude (DBIx::Class as a model). Unfortunately this is one of the many areas where I know how not to do it but I am not sure with the right way.

#### IOC (dependency injection) ####

The application follows the dependency injection pattern. The objects generally require their dependencies without creating them.
The complete application is wired by dependency injection Bread::Board. The containers are defined (and lazily loaded) from *.ioc files.

```
./app/lib/Ulmus/IOC/Model.ioc
./app/lib/Ulmus/IOC/Root.ioc
./app/lib/Ulmus/IOC/Webapp.ioc
./app/lib/Ulmus/IOC/Database.ioc
```

Different parts of the application can be reached from the root container

```
use Ulmus::IOC;

my $root = Ulmus::IOC->root;
$root->fetch('Database/storage')->get; # here comes the DBIx::Class::Storage
$root->fetch('Webapp/Mason/interp')->get; # here comes Mason interpreter
$root->fetch('Webapp/app')->get; # here comes the PSGI app
```
#### Configuration ####

There is a plain hash in `etc/Ulmus.yml` which is accessed from IOC.

### Further questions ###

If you need some further clarification or you have a problem running the application, feel free to contact me: [Roman Daniel](mailto:roman@daniel.cz)
