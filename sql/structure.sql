DROP TABLE IF EXISTS edge;
DROP TABLE IF EXISTS root_node;
DROP TABLE IF EXISTS node;
DROP TABLE IF EXISTS tree;

/* group of nodes */
CREATE TABLE tree (
  tree_id int(10) unsigned NOT NULL auto_increment,
  PRIMARY KEY (tree_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* node of tree */
CREATE TABLE node (
  node_id int(10) unsigned NOT NULL auto_increment,
  tree_id int(10) unsigned NOT NULL,
  depth   int(3)  unsigned NOT NULL,	 
  PRIMARY KEY (node_id),
  KEY (tree_id, node_id),
  FOREIGN KEY (tree_id) REFERENCES tree (tree_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* equivalent of document element in DOM */
CREATE TABLE root_node (
  tree_id int(10) unsigned NOT NULL,
  node_id int(10) unsigned NOT NULL,
  PRIMARY KEY (tree_id, node_id),
  UNIQUE KEY (tree_id),
  FOREIGN KEY (tree_id) REFERENCES tree (tree_id),
  FOREIGN KEY (tree_id, node_id) REFERENCES node (tree_id, node_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE edge (
  edge_id int(10) unsigned NOT NULL auto_increment,
  tree_id int(10) unsigned NOT NULL,
  parent_node_id int(10) unsigned NOT NULL,
  child_node_id int(10)  unsigned NOT NULL,
  PRIMARY KEY (edge_id),
  UNIQUE KEY (tree_id, child_node_id),
  FOREIGN KEY (tree_id) REFERENCES tree (tree_id),
  FOREIGN KEY (tree_id, parent_node_id) REFERENCES node (tree_id, node_id),
  FOREIGN KEY (tree_id, child_node_id) REFERENCES node (tree_id, node_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


